package com.gmail.gotoyuto7777.testteam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class Test extends JavaPlugin implements Listener{
	HashMap<String, Integer> data = new HashMap<String, Integer>();
	HashMap<String, Integer> point = new HashMap<String, Integer>();
	List<Player> redset = new ArrayList<Player>();
	List<Player> blueset = new ArrayList<Player>();
	String pf = ChatColor.AQUA+"[PointCount] " +ChatColor.YELLOW;
	ChatColor cgold = ChatColor.GOLD;
	ChatColor cred = ChatColor.GOLD;
	ChatColor cblue = ChatColor.GOLD;
	int task;

	Logger log;

	public void onEnable(){
		log = this.getLogger();
		log.info("起動成功！");
		this.saveDefaultConfig();
		point.put("赤チーム", 0);
		point.put("青チーム", 0);
		getServer().getPluginManager().registerEvents(this, this);
	}
	public void onDisable(){
		log.info("起動失敗！");
	}
	@EventHandler
	public void onArrowExplode(EntityExplodeEvent e) {
		e.blockList().clear();
	}
	@EventHandler
	public void BlockBreak(BlockBreakEvent e){
		Player p = e.getPlayer();
		Block b = e.getBlock();
		int bx = b.getX();
		int by = b.getY();
		int bz = b.getZ();
		if(b.getType()==Material.CHEST){
			if(p.getItemInHand().getType()==Material.STICK){
				if(redset.contains(p)){
					e.setCancelled(true);
					getConfig().set("チェスト.赤チーム.x", bx);
					getConfig().set("チェスト.赤チーム.y", by);
					getConfig().set("チェスト.赤チーム.z", bz);
					this.saveConfig();
					p.sendMessage(pf + "赤チームのチェストを設定しました。 "+ ChatColor.GRAY +"(x"+ bx +" y"+ by +" z"+ bz +")");
					redset.remove(p);
				}
				if(blueset.contains(p)){
					e.setCancelled(true);
					getConfig().set("チェスト.青チーム.x", bx);
					getConfig().set("チェスト.青チーム.y", by);
					getConfig().set("チェスト.青チーム.z", bz);
					this.saveConfig();
					p.sendMessage(pf + "青チームのチェストを設定しました。 "+ ChatColor.GRAY +"(x"+ bx +" y"+ by +" z"+ bz +")");
					blueset.remove(p);
				}
			}
		}
	}
	public void TestMesod(String st, Player p){
		Bukkit.broadcastMessage("メソッド作成テスト: "+ cred +""+ st +": "+ cblue +""+ p.getName());
		return;
	}
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("test")){
			Player p = (Player) sender;
			TestMesod(args[0],p);
		}
		if(cmd.getName().equalsIgnoreCase("chestset")){
			if(args[0].equalsIgnoreCase("red")){
				Player p = (Player) sender;
				ItemStack stick = new ItemStack(Material.STICK);
				ItemMeta stickm = stick.getItemMeta();
				stickm.setDisplayName("赤チェスト設定棒");
				List<String> lore = new ArrayList<String>();
				lore.add(cgold +"この棒で赤チームチェストに設定するチェストを");
				lore.add(cgold +"殴ってください。");
				redset.add(p);
				p.sendMessage(pf + "赤チームチェストを棒で殴ってください。");
			}
			if(args[0].equalsIgnoreCase("blue")){
				Player p = (Player) sender;
				ItemStack stick = new ItemStack(Material.STICK);
				ItemMeta stickm = stick.getItemMeta();
				stickm.setDisplayName("青チェスト設定棒");
				List<String> lore = new ArrayList<String>();
				lore.add(cgold +"この棒で青チームチェストに設定するチェストを");
				lore.add(cgold +"殴ってください。");
				blueset.add(p);
				p.sendMessage(pf + "青チームチェストを棒で殴ってください。");
			}
		}
		if(cmd.getName().equalsIgnoreCase("scoreshow")){
			if(args[0].equalsIgnoreCase("red")){
				Player p = (Player) sender;
				Location loc = p.getLocation();
				loc.setX(getConfig().getInt("チェスト.赤チーム.x"));
				loc.setY(getConfig().getInt("チェスト.赤チーム.y"));
				loc.setZ(getConfig().getInt("チェスト.赤チーム.z"));
				Chest ch = (Chest) loc.getBlock().getState();
				Inventory inv = ch.getBlockInventory();
				for (ItemStack it : inv.getContents() ) {
					if(it.getType().equals(Material.BRICK)){
						Bukkit.broadcastMessage("てすと： "+ it.getAmount());
					}
				}
			}
		}
		return false;
	}
}